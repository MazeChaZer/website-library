let pkgs = import (builtins.fetchTarball {
        name = "nixos-20.03-2b417708c282d84316366f4125b00b29c49df10f";
        url = "https://github.com/nixos/nixpkgs/archive/2b417708c282d84316366f4125b00b29c49df10f.tar.gz";
        sha256 = "0426qaxw09h0kkn5zwh126hfb2j12j5xan6ijvv2c905pqi401zq";
    }) {};
    racket2nix = pkgs.fetchFromGitLab {
        owner = "MazeChaZer"; repo = "racket2nix";
        # This revision is on the patch-pollen-repository-url branch
        rev = "a16e9b2afa805c0ccdcad8a8a6791cabcaeef790";
        sha256 = "0rvi2wa9m5l6nkmyp8d4p4mgyfgg2b0kpn8x8wlfnghx4dfjjmyl";
    };
in rec {
    pollen = (import racket2nix { package = "pollen"; }).lib;

    supplyPollenFiles = pkgs.writeShellScriptBin "supply-pollen-files" ''
        set -euo pipefail
        targetDirectory=$1
        for path in "pollen.rkt" "template.html.p" "style.css"; do
            cp "${./source-code}/$path" "$targetDirectory/$path"
            chmod +w "$targetDirectory/$path"
        done
    '';

    supplyPollenFilesForDirectory = documentationPath:
        pkgs.writeShellScriptBin "supply-pollen-files" ''
            set -euo pipefail
            ${supplyPollenFiles}/bin/supply-pollen-files "${documentationPath}"
        '';

    documentationPreview = documentationPath:
        pkgs.writeShellScriptBin "documentation-preview" ''
            set -euo pipefail
            ${supplyPollenFiles}/bin/supply-pollen-files "${documentationPath}"
            cd "${documentationPath}"
            ${pollen}/bin/raco pollen start
        '';

    shellDerivations = documentationPath: [
        pollen
        (supplyPollenFilesForDirectory documentationPath)
        (documentationPreview documentationPath)
    ];

    buildDocumentation = { projectName, documentation }:
        pkgs.stdenv.mkDerivation {
            name = "${projectName}-documentation";
            src = documentation;
            buildInputs = [ supplyPollenFiles pollen ];
            # One might think that `raco pollen render --recursive .` might be
            # better here, but recursive rendering mutates the cwd and thous
            # breaks a lot of things, so we are required to use pagetrees.
            # This means that only pages referenced by a pagetree are rendered.
            buildPhase = ''
                supply-pollen-files .
                raco pollen render *.ptree
            '';
            installPhase = ''
                raco pollen publish . "$out"
            '';
        };
}
